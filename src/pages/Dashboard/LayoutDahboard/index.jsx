import React from "react";
import ListCar from "../ListCar";
import AdminPage from "../AdminPage";
import SideNavTitle from "../../../components/SideNavTitle";
import Navbar from "../../../components/Navbar";
import SideNavItem from "../../../components/SideNavItem";
import { useLocation } from "react-router-dom";

const LayoutDashboard = () => {
  const location = useLocation();
  return (
    <div className="flex">
      <SideNavItem />
      <SideNavTitle />
      <div className="w-9/12 h-20 bg-white flex flex-col justify-between">
        <Navbar />
        {location.pathname === "/dashboard" ? <AdminPage /> : <ListCar />}
      </div>
    </div>
  );
};

export default LayoutDashboard;
