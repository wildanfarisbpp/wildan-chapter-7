import React from "react";
import HeaderDetail from "../../components/HeaderDetail";
import Ticket from "../../components/Ticket";

const InvoicePage = () => {
  return (
    <div>
      <HeaderDetail />
      <Ticket />
    </div>
  );
};

export default InvoicePage;
