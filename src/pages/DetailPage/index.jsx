import React from "react";
import HeaderDetail from "../../components/HeaderDetail";
import InputDetail from "../../components/InputDetail";
import CardDetail from "../../components/CardDetail";
import { useParams } from "react-router-dom";
import { useState, useEffect } from "react";
import axios from "axios";

const DetailPage = () => {
  const { id } = useParams();
  const [car, setCars] = useState([]);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(false);

  useEffect(() => {
    const fetchCar = async () => {
      setLoading(true);
      try {
        const res = await axios.get(`https://rent-cars-api.herokuapp.com/admin/car/${id}`);
        setCars(res.data);
        console.log(res.data);
        setLoading(false);
      } catch (error) {
        console.error(error);
        setError(true);
        setLoading(false);
      }
    };
    fetchCar();
  }, []);
  return (
    <>
      <HeaderDetail />
      <InputDetail />
      <div className="flex mx-44 mt-32">
        <div className="w-3/4 mr-6 pl-14 h-full rounded shadow-xl">
          <h5 className="font-bold mb-4">Tentang Paket</h5>
          <p className="font-light mb-2">Include</p>
          <div className="text-grayFont font-light pb-4">
            <li>Apa saja yang termasuk dalam paket misal durasi max 12 jam</li>
            <li>Sudah termasuk bensin selama 12 jam</li>
            <li>Sudah termasuk Tiket Wisata</li>
            <li>Sudah termasuk pajak</li>
          </div>

          <p className="font-light mb-2">Exclude</p>
          <div className="text-grayFont font-light pb-4">
            <li>Tidak termasuk biaya makan sopir Rp 75.000/hari</li>
            <li>Jika overtime lebih dari 12 jam akan ada tambahan biaya Rp 20.000/jam</li>
            <li>Tidak termasuk akomodasi penginapan</li>
          </div>

          <h5 className="font-bold my-4">Refund, Reschedule, Overtime</h5>
          <div className="text-grayFont font-light pb-4">
            <li>Tidak termasuk biaya makan sopir Rp 75.000/hari</li>
            <li>Jika overtime lebih dari 12 jam akan ada tambahan biaya Rp 20.000/jam</li>
            <li>Tidak termasuk akomodasi penginapan</li>
            <li>Tidak termasuk biaya makan sopir Rp 75.000/hari</li>
            <li>Jika overtime lebih dari 12 jam akan ada tambahan biaya Rp 20.000/jam</li>
            <li>Tidak termasuk akomodasi penginapan</li>
            <li>Tidak termasuk biaya makan sopir Rp 75.000/hari</li>
            <li>Jika overtime lebih dari 12 jam akan ada tambahan biaya Rp 20.000/jam</li>
            <li>Tidak termasuk akomodasi penginapan</li>
          </div>
        </div>
        <div className="flex flex-col shadow-xl h-full pb-10">
          <CardDetail carData={car} />
          <a href="/invoice" className="py-2 mt-4 text-white font-semibold text-center rounded mx-10 bg-alert-success">
            Lanjutkan Pembayaran
          </a>
        </div>
      </div>
      <div className="w-3/4 mt-6 pr-28 flex justify-end">
        <a href="/invoice" className="px-4 py-2 mr-20 mb-10 text-white font-semibold text-center rounded bg-alert-success">
          Lanjutkan Pembayaran
        </a>
      </div>
    </>
  );
};

export default DetailPage;
