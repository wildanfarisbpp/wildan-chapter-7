import React from "react";
import "./Style.css";
import { VictoryPie } from "victory";
import { Link } from "react-router-dom";

const HomePage = () => {
  return (
    <div>
      <link rel="stylesheet" href="https://unpkg.com/swiper@8/swiper-bundle.min.css" />
      <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous" />
      {/* <!-- Navabar --> */}
      <nav className="navbar navbar-expand-lg navbar-light">
        <div className="container">
          <a className="navbar-brand" href="#"></a>
          <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarNav">
            <ul className="navbar-nav ml-auto">
              <li className="nav-item active">
                <a className="nav-link" href="#our-services">
                  Our Services <span className="sr-only">(current)</span>
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="#why-us">
                  Why Us
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="#testimonial">
                  Testimonial
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="#faq">
                  FAQ
                </a>
              </li>
              <li className="nav-item">
                <button className="btn btn-success tombol" href="#">
                  Register
                </button>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      {/* <!-- Akhir Navbar --> */}

      <div className="container-fluid hero">
        <div className="container">
          <div className="row">
            <div className="col hero-section">
              <div className="row">
                <div className="col-lg hero-section-left">
                  <h2>Sewa & Rental Mobil Terbaik di kawasan (Lokasimu)</h2>
                  <p>Selamat datang di Binar Car Rental. Kami menyediakan mobil kualitas terbaik dengan harga terjangkau. Selalu siap melayani kebutuhanmu untuk sewa mobil selama 24 jam.</p>
                  <Link to="/list" className="btn btn-success tombol">
                    Mulai Sewa Mobil
                  </Link>
                </div>
                <div className="col-lg hero-section-right">
                  <img src="/img_car.png" alt="car image" className="img-fluid" />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      {/* <!-- Our Services --> */}
      <div className="container" id="our-services">
        <div className="row our-services">
          <div className="col left-image img-fluid">
            <img src="/img_service.svg" alt="our-services" />
          </div>
          <div className="col">
            <h3>Best Car Rental for any kind of trip in (Lokasimu)!</h3>
            <p>Sewa mobil di (Lokasimu) bersama Binar Car Rental jaminan harga lebih murah dibandingkan yang lain, kondisi mobil baru, serta kualitas pelayanan terbaik untuk perjalanan wisata, bisnis, wedding, meeting, dll.</p>
            <p>
              <span>
                <img src="/check.svg" alt="check" />
              </span>
              Sewa Mobil Dengan Supir di Bali 12 Jam
            </p>
            <p>
              <span>
                <img src="/check.svg" alt="check" />
              </span>
              Sewa Mobil Lepas Kunci di Bali 24 Jam
            </p>
            <p>
              <span>
                <img src="/check.svg" alt="check" />
              </span>
              Sewa Mobil Jangka Panjang Bulanan
            </p>
            <p>
              <span>
                <img src="/check.svg" alt="check" />
              </span>
              Gratis Antar - Jemput Mobil di Bandara
            </p>
            <p>
              <span>
                <img src="/check.svg" alt="check" />
              </span>
              Layanan Airport Transfer / Drop In Out
            </p>
          </div>
        </div>
      </div>
      {/* <!-- Akhir Our Services --> */}

      {/* <!-- Why Us? --> */}
      <div className="container" id="why-us">
        <div className=" why-us-title">
          <h3>Why Us?</h3>

          <p>Mengapa harus pilih Binar Car Rental?</p>
        </div>

        {/* <!-- Why Us Card --> */}
        <div className="row why-us-card">
          <div className="col">
            <div className="card">
              <img src="/icon_complete.svg" width="32" alt="icon_price" />
              <div className="card-body">
                <h4>Mobil Lengkap</h4>
                <p className="card-text">Tersedia banyak pilihan mobil, kondisi masih baru, bersih dan terawat</p>
              </div>
            </div>
          </div>
          <div className="col">
            <div className="card">
              <img src="/icon_price.svg" width="32" alt="icon_price" />
              <div className="card-body">
                <h4>Harga Murah</h4>
                <p className="card-text">Harga murah dan bersaing, bisa bandingkan harga kami dengan rental mobil lain</p>
              </div>
            </div>
          </div>
          <div className="col">
            <div className="card">
              <img src="/icon_24hrs.svg" width="32" alt="icon_price" />
              <div className="card-body">
                <h4>Layanan 24 Jam</h4>
                <p className="card-text">Siap melayani kebutuhan Anda selama 24 jam nonstop. Kami juga tersedia di akhir minggu</p>
              </div>
            </div>
          </div>
          <div className="col">
            <div className="card">
              <img src="/icon_professional.svg" width="32" alt="icon_price" />
              <div className="card-body">
                <h4>Sopir Profesional</h4>
                <p className="card-text">Sopir yang profesional, berpengalaman, jujur, ramah dan selalu tepat waktu</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* <!-- Akhir why us --> */}
      <h3 className="text-center text-md mt-6">Beberapa mobil paling banyak disewa!</h3>
      <VictoryPie
        height={200}
        colorScale={["gold", "cyan", "navy"]}
        data={[
          { x: "Avanza", y: 35 },
          { x: "Ayla", y: 40 },
          { x: "Innova", y: 55 },
        ]}
      />

      {/* <!-- CTA Banner --> */}
      <div className="container ">
        <div className="cta-banner">
          <h3>Sewa Mobil di (Lokasimu) Sekarang</h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
          <a href="" className="btn btn-success tombol">
            Mulai Sewa Sekarang
          </a>
        </div>
      </div>
      {/* <!-- Akhir CTA Banner --> */}

      {/* <!-- FAQ --> */}
      <div className="container" id="faq">
        <div className="row faq ">
          <div className="col">
            <h3>Frequently Asked Question</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
          </div>
          <div className="col">
            {/* <!-- Accordion --> */}
            <div className="accordion" id="accordionExample">
              <div className="card">
                <div className="card-header " id="headingOne">
                  <h2 className="mb-0">
                    <button className="btn btn-block text-left" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                      Apa saja syarat yang dibutuhkan?
                    </button>
                  </h2>
                </div>

                <div id="collapseOne" className="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                  <div className="card-body">Lorem ipsum dolor sit amet consectetur adipisicing elit. Non, explicabo!</div>
                </div>
              </div>
              <div className="card">
                <div className="card-header" id="headingTwo">
                  <h2 className="mb-0">
                    <button className="btn btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                      Berapa hari minimal sewa mobil lepas kunci?
                    </button>
                  </h2>
                </div>
                <div id="collapseTwo" className="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                  <div className="card-body">Some placeholder content for the second accordion panel. This panel is hidden by default.</div>
                </div>
              </div>
              <div className="card">
                <div className="card-header" id="headingThree">
                  <h2 className="mb-0">
                    <button className="btn btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                      Berapa hari sebelumnya sabaiknya booking sewa mobil?
                    </button>
                  </h2>
                </div>
                <div id="collapseThree" className="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                  <div className="card-body">And lastly, the placeholder content for the third and final accordion panel. This panel is hidden by default.</div>
                </div>
              </div>
              <div className="card">
                <div className="card-header" id="headingFour">
                  <h2 className="mb-0">
                    <button className="btn btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                      Apakah Ada biaya antar-jemput?
                    </button>
                  </h2>
                </div>
                <div id="collapseFour" className="collapse" aria-labelledby="headingFour" data-parent="#accordionExample">
                  <div className="card-body">And lastly, the placeholder content for the third and final accordion panel. This panel is hidden by default.</div>
                </div>
              </div>
              <div className="card">
                <div className="card-header" id="headingFive">
                  <h2 className="mb-0">
                    <button className="btn btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                      Bagaimana jika terjadi kecelakaan?
                    </button>
                  </h2>
                </div>
                <div id="collapseFive" className="collapse" aria-labelledby="headingFive" data-parent="#accordionExample">
                  <div className="card-body">And lastly, the placeholder content for the third and final accordion panel. This panel is hidden by default.</div>
                </div>
              </div>
            </div>
            {/* <!-- Akhir Accordion --> */}
          </div>
        </div>
      </div>
      {/* <!-- Akhir FAQ --> */}

      {/* <!-- Footer --> */}
      <div className="container">
        <div className="row footer">
          <div className="col">
            <p>Jalan Suroyo No. 161 Mayangan Kofta Probolonggo 672000</p>
            <p>binarcarrental@gmail.com</p>
            <p>081-233-334-808</p>
          </div>
          <div className="col">
            <p> Our services</p>
            <p>Why Us</p>
            <p>Testimonial</p>
            <p>FAQ</p>
          </div>
          <div className="col col-social-media">
            <p>Connect with us</p>
            <div className="row social-media">
              <img src="/icon_facebook.svg" alt="icon_facebook" />
              <img src="/icon_instagram.svg" alt="icon_instagram" />
              <img src="/icon_twitter.svg" alt="icon_twitter" />
              <img src="/icon_mail.svg" alt="icon_mail" />
              <img src="/icon_twitch.svg" alt="icon_twitch" />
            </div>
          </div>
          <div className="col">
            <p>Copyright Binar 2022</p>
            <img src="/icon_blue.svg" alt="icon_blue" />
          </div>
        </div>
      </div>
      {/* <!-- Akhir Footer --> */}

      {/* <!-- Optional JavaScript; choose one of the two! --> */}

      {/* <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) --> */}
      <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-fQybjgWLrvvRgtW6bFlB7jaZrFsaBXjsOMm/tB9LTS58ONXgqbR9W8oWht/amnpF" crossorigin="anonymous"></script>
    </div>
  );
};

export default HomePage;
