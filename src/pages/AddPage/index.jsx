import React from "react";
import SideNavItem from "../../components/SideNavItem";
import SideNavTitle from "../../components/SideNavTitle";
import Navbar from "../../components/Navbar";
import Input from "../../components/Input";
import BreadCrumb from "../../components/Breadcrumb";

const AddPage = () => {
  return (
    <div className="flex">
      <SideNavItem />
      <SideNavTitle />
      <div className="w-9/12 h-20 flex flex-col justify-between">
        <Navbar />
        <div className="w-full min-h-[88.9vh] bg-[#f4f5f7]">
          <BreadCrumb />
          <h2 className="font-bold text-lg ml-6 mb-4">Add New Car</h2>
          <Input />
        </div>
      </div>
    </div>
  );
};

export default AddPage;
