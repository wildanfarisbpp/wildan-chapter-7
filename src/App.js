import HomePage from "./pages/HomePage";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import LayoutDashboard from "./pages/Dashboard/LayoutDahboard";
import AddPage from "./pages/AddPage";
import DetailPage from "./pages/DetailPage";
import InvoicePage from "./pages/InvoicePage";
import "antd/dist/antd.min.css";
function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<HomePage />} />
          <Route path="dashboard" element={<LayoutDashboard />} />
          <Route path="list" element={<LayoutDashboard />} />
          <Route path="/list/:id" element={<DetailPage />} />
          <Route path="add" element={<AddPage />} />
          <Route path="invoice" element={<InvoicePage />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
