const InputDetail = () => {
  return (
    <div className="w-full h-full bg-[#F1F3FF] pt-28 flex-col">
      <form>
        <div className="w-3/4 py-5 bg-white shadow-md ml-[13vw] rounded-md z-10 -mt-20 absolute">
          <div className="flex items-end justify-start">
            <div className="mx-4">
              <p className="mb-4 font-bold text-lg">Pencarianmu</p>
              <p className="text-left mb-3 font-light">Tipe Driver</p>
              <input
                disabled
                className=" w-64 px-2 py-2 bg-neutral-neutral2 text-[#3C3C3C] font-light form-select rounded transition ease-in-out border border-solid border-gray-400 m-0 focus:text-gray-700 focus:bg-white focus:border-success focus:outline-none"
              />
            </div>

            <div className="mx-3">
              <p className="text-left mb-3 font-light">Tangal</p>
              <input
                disabled
                className=" bg-neutral-neutral2 w-64 px-2 py-2 text-[#3C3C3C] font-light form-select rounded transition ease-in-out border border-solid border-gray-400 m-0 focus:text-gray-700 focus:bg-white focus:border-success focus:outline-none"
              />
            </div>
            <div className="mx-3">
              <p className="text-left mb-3 font-light">Waktu Jemput/Ambil</p>
              <input
                disabled
                className="w-64 px-2 py-2 bg-neutral-neutral2 text-[#3C3C3C] font-light form-select rounded transition ease-in-out border border-solid border-gray-400 m-0 focus:text-gray-700 focus:bg-white focus:border-success focus:outline-none"
              />
            </div>

            <div className="mx-3">
              <p className="text-left mb-3 font-light ">Jumlah Penumpang (optional)</p>
              <div>
                <input
                  disabled
                  className="w-64 px-2 py-2 bg-neutral-neutral2 text-[#3C3C3C] font-light form-select rounded transition ease-in-out border border-solid border-gray-400 m-0 focus:text-gray-700 focus:bg-white focus:border-success focus:outline-none"
                />
              </div>
            </div>
          </div>
        </div>
      </form>
    </div>
  );
};

export default InputDetail;
