import axios from "axios";
import React from "react";
import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { message } from "antd";

const Input = () => {
  const [name, setName] = useState("");
  const [price, setPrice] = useState(0);
  const [image, setImage] = useState("");
  const navigate = useNavigate();

  const success = () => {
    message.success("Data Berhasil Ditambahkan!");
  };

  const postCar = async (e) => {
    e.preventDefault();

    const formData = new FormData();
    formData.append("name", name);
    formData.append("category", "Small");
    formData.append("price", price);
    formData.append("image", image);
    const config = {
      headers: {
        "content-type": "multipart/form-data",
      },
    };

    try {
      await axios.post(`https://rent-cars-api.herokuapp.com/admin/car`, formData, config).then((response) => {
        console.log(response);
        success();
        navigate("/list");
      });
    } catch (error) {
      console.log(error);
      alert("Can't Post the data!");
    }
  };

  return (
    <div className="bg-white ml-6 py-4 mr-6">
      <div className="flex flex-col">
        <form onSubmit={postCar}>
          <label for="nama" className="inline-block mb-1 pl-4 pr-24 text-sm text-black font-light">
            Nama
            <span className="text-red-500">*</span>
          </label>
          <input type="text" name="nama" className="w-1/3 px-4 py-1 text-base border border-gray-300 rounded outline-none  focus:border-black " placeholder="Nama" onChange={(e) => setName(e.target.value)} />
          <div className="block mt-5">
            <label for="harga" className="inline-block mb-1 pl-4 pr-24 text-sm text-black font-light">
              Harga
              <span className="text-red-500">*</span>
            </label>
            <input type="number" name="harga" className="w-1/3 px-4 py-1 text-base border border-gray-300 rounded outline-none  focus:border-black " placeholder="Harga" onChange={(e) => setPrice(e.target.value)} />
          </div>
          <div className="block mt-5">
            <label for="foto" className="inline-block mb-1 pl-4 pr-24 text-sm text-black font-light">
              Foto
              <span className="text-red-500">*</span>
            </label>
            <input type="file" name="foto" className="w-1/3 px-4 py-1 ml-2 text-base border border-gray-300 rounded outline-none  focus:border-black" placeholder="Foto" accept="image/*" onChange={(e) => setImage(e.target.files[0])} />
            <span className="block text-sm ml-40 font-light text-neutral-neutral3 mt-1">File size max. 2MB</span>
          </div>
          <div className="block mt-5">
            <label className="inline-block mb-1 pl-4 pr-24 text-sm text-black font-light">Start Rent</label>
            <span>-</span>
          </div>
          <div className="block mt-5">
            <label className="inline-block mb-1 pl-4 pr-24 text-sm text-black font-light">Finish Rent</label>
            <span>-</span>
          </div>
          <div className="block mt-5">
            <label className="inline-block mb-1 pl-4 pr-24 text-sm text-black font-light">Created at</label>
            <span>-</span>
          </div>
          <div className="block mt-5">
            <label className="inline-block mb-1 pl-4 pr-24 text-sm text-black font-light">Updated at</label>
            <span>-</span>
          </div>
          <div className=" absolute z-10 mt-20">
            <a href="/list" className="px-4 py-2 mr-4 border-2 border-primary-darkblue4 text-primary-darkblue4 font-semibold rounded">
              Cancel
            </a>
            <button type="submit" className="px-4 py-2 mr-4 border-2  border-primary-darkblue4 bg-primary-darkblue4 text-white font-semibold rounded">
              Save
            </button>
          </div>
        </form>
      </div>
    </div>
  );
};

export default Input;
