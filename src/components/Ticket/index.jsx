import React from "react";
import { ArrowLeftOutlined, CheckOutlined, DownloadOutlined } from "@ant-design/icons";
import file from "../../assets/invoice.pdf";
import { Worker, Viewer } from "@react-pdf-viewer/core";
import { getFilePlugin } from "@react-pdf-viewer/get-file";

const Ticket = () => {
  const getFilePluginInstance = getFilePlugin();
  const { Download } = getFilePluginInstance;

  return (
    <>
      <div className="w-full h-full pt-6 bg-[#F1F3FF] px-52">
        <div className="flex justify-between ">
          <div>
            <ArrowLeftOutlined style={{ fontSize: "18px" }} />
            <div className="flex-col ml-4 inline-flex">
              <p className="font-semibold text-lg ">Tiket</p>
              <p className="text-sm font-light mt-1 mb-4 ">Order ID xxxxxxx</p>
            </div>
          </div>
          <div className="flex">
            <div className="bg-primary-darkblue4 w-8 h-1  px-1 pb-8 rounded-full">
              <CheckOutlined style={{ color: "white", fontSize: "16px", paddingLeft: "4px" }} />
            </div>
            <p className="mt-1 ml-2">Pilih Metode</p>
            <div className="h-[2px] w-10 bg-primary-darkblue4 mx-2 my-4"></div>
            <div className="bg-primary-darkblue4 w-8 h-1  px-1 pb-8 rounded-full">
              <CheckOutlined style={{ color: "white", fontSize: "16px", paddingLeft: "4px" }} />
            </div>
            <p className="mt-1 ml-2">Bayar</p>
            <div className="h-[2px] w-10 bg-primary-darkblue4 mx-2 my-4"></div>
            <div className="bg-primary-darkblue4 w-8 h-1 px-1 pb-8 rounded-full">
              <p className="text-white text-xl font-normal ml-2">3</p>
            </div>
            <p className="mt-1 ml-2">Tiket</p>
          </div>
        </div>
      </div>

      <div className="flex flex-col items-center">
        <div className="bg-alert-success w-20 h-20 mt-12 mb-6 rounded-full">
          <CheckOutlined style={{ fontSize: "40px", color: "white", marginTop: "20px", marginLeft: "20px" }} />
        </div>
        <p className="font-bold text-xl">Pembayaran Berhasil!</p>
        <p className="text-lg mt-2 mb-6 text-gray-400">Tunjukan invoice ini ke petugas BCR di titik temu</p>
        <div className="w-2/4 h-[650px] shadow-xl">
          <div className="flex my-6 mx-6 justify-between">
            <div className="flex flex-col ">
              <p className="text-lg font-semibold">Invoice</p>
              <p className="my-2 text-gray-500">*no invoice</p>
            </div>
            <div>
              <Download>
                {(props) => (
                  <a className="px-2 py-2 border-2 border-primary-darkblue4 rounded hover:cursor-pointer" onClick={props.onClick}>
                    <DownloadOutlined style={{ color: "blue", fontSize: "20px" }} />
                    <p className="inline-block ml-2 text-base text-primary-darkblue4 font-bold">Unduh</p>
                  </a>
                )}
              </Download>
            </div>
          </div>
          <div className="w-full h-full mx-6">
            <Worker workerUrl="https://unpkg.com/pdfjs-dist@2.13.216/build/pdf.worker.min.js">
              <div
                style={{
                  border: "1px solid rgba(0, 0, 0, 0.3)",
                  height: "450px",
                  width: "700px",
                }}
              >
                <Viewer fileUrl={file} plugins={[getFilePluginInstance]} />
              </div>
            </Worker>
          </div>
        </div>
      </div>
    </>
  );
};

export default Ticket;
