import React from "react";
import { MenuOutlined } from "@ant-design/icons";

const Navbar = () => {
  return (
    <div className="flex justify-between">
      <MenuOutlined style={{ color: "black", marginTop: "40px", fontSize: "20px", marginLeft: "24px" }} />
      <div className="mr-8 my-6 flex">
        <img src="/fi_profile.png" alt="profile" className="rounded-full h-8 w-8" />
        <p className="font-light ml-2 text-lg">Unis Badri</p>
      </div>
    </div>
  );
};

export default Navbar;
