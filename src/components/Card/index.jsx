import React from "react";
import { Link } from "react-router-dom";

const Card = ({ carsData }) => {
  return (
    <>
      {carsData.map((data, index) => (
        <div className="bg-white rounded-lg w-[21.7rem] my-2 mx-2">
          <Link to={`${data.id}`}>
            <img src={data.image} alt="innova" width={"200px"} className=" pt-12 mx-auto" />
          </Link>
          <div className="px-6 pb-6">
            <p className="font-semibold mb-2 mt-8">
              {data.name}/{data.category}
            </p>
            <p className="mb-4 font-bold text-lg">Rp {data.price} / hari</p>
            <p className="my-2">
              <span className="mr-2">
                <img src="/fi_key.png" alt="key" className="inline" />
              </span>
              Start rent - Finish rent
            </p>

            <p className=" mt-4">
              <span className="mr-2">
                <img src="/fi_clock.png" alt="key" className="inline" />
              </span>
              Updated at 4 Apr 2022, 09.00
            </p>
          </div>
          <div className="flex justify-center mb-6">
            <a href="#" className="px-9 py-2 text-alert-danger font-bold border-2 border-alert-danger rounded mr-2 ">
              <span className="mr-2">
                <img src="/fi_trash.png" alt="fi trash" className="inline" />
              </span>
              Delete
            </a>

            <a href="#" className="px-9 py-2 bg-alert-success text-white font-bold border-2 border-alert-success rounded">
              <span className="mr-2">
                <img src="/fi_edit.png" alt="fi trash" className="inline" />
              </span>
              Edit
            </a>
          </div>
        </div>
      ))}
    </>
  );
};

export default Card;
