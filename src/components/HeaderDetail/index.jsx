import React from "react";
import { Link } from "react-router-dom";

const HeaderDetail = () => {
  return (
    <nav className="bg-[#F1F3FF]">
      <div className="mx-44 flex justify-between">
        <Link to="/">
          <div className="my-6 px-12 py-5 bg-primary-darkblue4"></div>
        </Link>
        <div className="text-black font-semibold text-md flex items-center">
          <p className="mx-4 font-semibold ">Our Services</p>
          <p className="mx-4 font-semibold ">Why Us</p>
          <p className="mx-4 font-semibold ">Testimonial</p>
          <p className="mx-4 font-semibold ">FAQ</p>
          <div className=" bg-primary-limegreen4 px-5 py-2 rounded-sm hover:shadow-2xl hover:bg-green-600">
            <button className="text-white font-semibold">Register</button>
          </div>
        </div>
      </div>
    </nav>
  );
};

export default HeaderDetail;
